package mux

import (
	"net/http"
)

type CustomeHandler func(http.ResponseWriter, *http.Request)

type MuxMaurick struct {
	muxPaths map[string]CustomeHandler //handlers
}

func (this *MuxMaurick) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "hello")
	if fn, ok := this.muxPaths[r.URL.Path]; ok {
		fn(w, r)
		return
	}
	http.NotFound(w, r)
}

func (this *MuxMaurick) AddMuxFunc(path string, fn CustomeHandler) {
	this.muxPaths[path] = fn
}

func (this *MuxMaurick) AddMuxHandler(path string, handler http.Handler) {
	this.muxPaths[path] = handler.ServeHTTP
}

func CreateMux() *MuxMaurick {
	var newMap = make(map[string]CustomeHandler)
	mux := &MuxMaurick{
		muxPaths: newMap,
	}
	return mux
}
