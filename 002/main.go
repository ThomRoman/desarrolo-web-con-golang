package main

import (
	"fmt"
	"log"
	"net/http"
)

// GET POST PUT DELETE

func main() {

	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		fmt.Println("El método es " + req.Method)
		// fmt.Fprintf(res, "METHOD "+req.Method)
		switch req.Method {
		case "GET":
			fmt.Fprintf(res, "GET")
		case "POST":
			fmt.Fprintf(res, "GET")
		case "PUT":
			fmt.Fprintf(res, "GET")
		case "DELETE":
			fmt.Fprintf(res, "GET")
		default:
			http.Error(res, "Metodo no valido ", http.StatusBadRequest)
		}

	})

	log.Fatal(http.ListenAndServe(":3000", nil))
}
