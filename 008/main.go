package main

import (
	"log"
	"net/http"
)

func redirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://www.google.com.mx", http.StatusMovedPermanently)
}

func notFound(w http.ResponseWriter, r *http.Request) {
	http.NotFound(w, r)
}

func main() {
	http.HandleFunc("/redirect", redirect)
	// redirect := http.RedirectHandler("https://www.google.com.mx",http.StatusMovedPermanently)
	// http.HandleFunc("/redirect", redirect)
	http.HandleFunc("/not-found", notFound)
	// notfound := http.NotFoundHandler()
	// http.HandleFunc("/not-found", notfound)

	server := &http.Server{
		Addr:    "localhost:3000",
		Handler: nil,
	}
	log.Fatal(server.ListenAndServe())
}
