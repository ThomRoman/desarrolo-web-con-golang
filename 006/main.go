package main

import (
	"fmt"
	"log"
	"net/http"
)

/*
type Handler interface {
	ServerHTTP(ResponseWritter,*Request)
}
*/

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/", handleMux)
	mux.HandleFunc("/dos", handleMux)

	http.HandleFunc("/", handle) // defautlServeMux

	server := &http.Server{
		Addr: "localhost:3000",
		// Handler: nil, // defautlServeMux
		Handler: mux, // usamos nuesto propio mux
	}
	log.Fatal(server.ListenAndServe())
}

func handleMux(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Custom Mux")
}
func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "DefaultServeMux")
}
