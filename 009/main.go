package main

import (
	"fmt"
	"log"
	"net/http"
)

type customeHandler func(http.ResponseWriter, *http.Request)

type MuxMaurick struct {
	muxPaths map[string]customeHandler //handlers
}

func (this *MuxMaurick) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "hello")
	fn := this.muxPaths[r.URL.Path]
	fn(w, r)
}

func (this *MuxMaurick) AddMux(path string, fn customeHandler) {
	this.muxPaths[path] = fn
}

func main() {

	var newMap = make(map[string]customeHandler)
	mux := &MuxMaurick{
		muxPaths: newMap,
	}

	mux.AddMux("/hello", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "hello")
	})

	mux.AddMux("/world", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "world")
	})

	// log.Fatal(http.ListenAndServe("localhost:3000", nil)) // nil deaultMuxServer
	log.Fatal(http.ListenAndServe("localhost:3000", mux)) // mux => un listado de rutas que tienen asociados funciones
}
