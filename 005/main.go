package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", handle) // lo que hace esta funcion es alamacenar la ruta y el manejador
	// en una variable que es DefaultServeMux

	// DefaultServeMux : es un listado de rutas son acciones a realizar

	log.Fatal(http.ListenAndServe(":3000", nil)) // cuando se coloca nil se le está dificiendo que use como
	// serverMux a DefaultMux

	// otra opcion
	// server := &http.Server{
	// 	Addr:    "localhost:3000",
	// 	Handler: nil, // si es nil utilizamos default server mux
	// }
	// log.Fatal(server.ListenAndServe())

	// recuerda : siempre que trabajemos con servidores en go necesitaremos utilizar server mux y este
	// no es mas que un listado de rutas
}

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello")
}
