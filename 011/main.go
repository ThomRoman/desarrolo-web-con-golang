package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type Course struct {
	Name string
	Time int
}

type User struct {
	UserName string
	Age      int
	Active   bool
	Admin    bool
	Skills   []string
	Courses  []Course
}

func (this User) HasAdminPermissions(key string) bool {
	return this.Active && this.Admin && key == "yes"
}
func (this User) HasPermission(feature string) bool {
	if feature == "feature-a" {
		return true
	} else {
		return false
	}
}

func hello() string {
	return "hello world"
}
func sum(number1, number2 int) int {
	return number1 + number2
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// template, err := template.New("Hello").Parse("Hello world")
		functions := template.FuncMap{
			"hello": hello,
			"sum":   sum,
		}
		// template, err := template.ParseFiles("templates/index.html")
		// template, err := template.New("index.html").Funcs(functions).ParseFiles("templates/index.html", "templates/footer.html")
		template := template.Must(template.New("index.html").Funcs(functions).ParseFiles("templates/index.html", "templates/footer.html"))
		// if err != nil {
		// panic(err)
		// }
		// template.Execute(w, nil) // el primer parametro sera la respuesta del servidor al cliente cuando se hizo el request
		// el segundo parametro es una estructura la cual nos ayudará a generar páginas web dinámicas
		// al comienzo a tal estructura le coloqué nil solo para obtener una pagina estática mediante un template

		skills := []string{"Go", "Python", "Java", "Javascript"}
		courses := []Course{
			Course{
				Name: "Go desde cero",
				Time: 24,
			},
			Course{
				Name: "Rust desde cero",
				Time: 72,
			},
			Course{
				Name: "Sass desde cero",
				Time: 128,
			},
		}
		user := &User{"thom", 22, true, true, skills, courses}

		template.Execute(w, user)
	})

	fmt.Println(":3000")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
