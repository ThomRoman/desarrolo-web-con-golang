package main

import (
	"log"
	"net/http"

	"./handlers"
	"github.com/gorilla/mux"
)

func main() {

	mux := mux.NewRouter()
	mux.HandleFunc("/api/v1/users", handlers.GetUsers).Methods("GET")
	mux.HandleFunc("/api/v1/users/{id:[0-9]+}", handlers.GetUserById).Methods("GET")
	mux.HandleFunc("/api/v1/users", handlers.CreateUser).Methods("POST")
	mux.HandleFunc("/api/v1/users/{id:[0-9]+}", handlers.UpdateUserById).Methods("PUT")
	mux.HandleFunc("/api/v1/users/{id:[0-9]+}", handlers.DeleteUserById).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", mux))
}
