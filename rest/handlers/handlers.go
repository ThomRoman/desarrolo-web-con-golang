package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"../models"
	"github.com/gorilla/mux"
)

// mimes types
// Accept : quiere decir lo que el cliente acepta
// 	text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
// y luego le mandamos el content type es decir lo que le devolvio el servidor
var mimeTypes = []string{"application/json", "text/plain", "text/xml"}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "get users")
	models.SendData(w, models.GetUsers())
}

func searchMime(arr []string, mimeType string) int {
	for index, mime := range arr {
		if mime == mimeType {
			return index
		}
	}
	return -1
}
func GetUserById(w http.ResponseWriter, r *http.Request) {

	// mimeType :=
	// accept := r.Header.Get("Accept")

	// if index := searchMime(mimeTypes, accept); index == -1 {
	// 	return
	// }
	params := mux.Vars(r)
	userId, _ := strconv.Atoi(params["id"]) // esto retorna un string , hay que convertirlo a int
	response := models.GetDefaultResponse(w)
	user, err := models.GetUser(userId)
	if err != nil {
		w.WriteHeader(http.StatusNoContent)
		response.NotFound(err.Error())
	} else {
		response.Data = user
	}
	response.Send()
	// user := &models.User{
	// 	Id:       1,
	// 	Username: "MaurickThom",
	// 	Password: "12345",
	// }

	// js, err := json.Marshal(&user)
	// yaml, err := yaml.Marshal(&user)
	// xml, err := xml.Marshal(&user)
	// xml, err := xml.Marshal(&response)
	// if err != nil {
	// 	return
	// }
	// w.Header().Set("Content-Type", "text/xml")
	// w.Header().Set("Content-Type", "application/json")
	// w.Header().Set("Content-Type", "yaml") esto no se pone cuando es yaml
	// paquete de yaml gopkg.in/yaml.v2
	// fmt.Fprintf(w, "get user by id")
	// w.WriteHeader(http.StatusOK)
	// w.Write(xml)
	// w.Write([]byte(user.String())) // text/plain
	// fmt.Fprintf(w, string(js))

}
func CreateUser(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "create user")
}
func UpdateUserById(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "update user")
}
func DeleteUserById(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Delete by id")
}
