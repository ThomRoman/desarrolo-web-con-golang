package models

import (
	"errors"
	"fmt"
)

type User struct {
	Id       int    `json:"id" xml:"id"` // esto le dirá que cuando se seralizen en formato json
	Username string `json:"username"`
	Password string `json:"password"`
}

var users = make(map[int]User)

type Users []*User

func (user *User) String() string {
	var r string
	r = fmt.Sprintf("%d, %s,%s, %s", user.Id, user.Username, user.Password)
	return r
}

func (users *Users) String() string {
	var r string
	for _, user := range *users {
		r += fmt.Sprintf("%d, %s,%s, %s", user.Id, user.Username, user.Password)
	}
	return r
}

func SetDefaultUser() {
	user := User{
		Id:       1,
		Username: "asdas",
		Password: "asdasd",
	}
	users[user.Id] = user
}

func GetUser(userId int) (User, error) {
	if user, ok := users[userId]; ok {
		return user, nil
	}
	return User{}, errors.New("El usuario no se encuentra dentro del mapa")
}

func GetUsers() Users {
	list := Users{}
	for _, user := range users {
		list = append(list, &user) // otra solucion del & es quitar el puntero * en el typo de dato
	}
	return list
}
