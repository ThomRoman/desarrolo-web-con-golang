package models

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Response struct {
	Status      int         `json:"status"`
	Data        interface{} `json:"data"`
	Message     string      `json:"message"`
	contentType string
	write       http.ResponseWriter
}

func GetDefaultResponse(w http.ResponseWriter) Response {
	return Response{
		Status:      http.StatusOK,
		write:       w,
		contentType: "application/json",
	}
}

func (this *Response) NotFound(message string) {
	this.Status = http.StatusNotFound
	this.Data = nil
	this.Message = message
}

func (this *Response) Send() {
	this.write.Header().Set("Content-Type", this.contentType)
	this.write.WriteHeader(this.Status)
	output, _ := json.Marshal(&this)
	fmt.Fprintf(this.write, string(output))
}

func SendData(w http.ResponseWriter, data interface{}) {
	response := GetDefaultResponse(w)

	response.Data = data
	response.Send()
}

func SendNotFound(w http.ResponseWriter) {
	// response := GetDefaultResponse(w)
	// response.NotFound()
}
