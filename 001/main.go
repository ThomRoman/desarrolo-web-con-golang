package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Name", "Name value")
		// fmt.Fprintf(res, "hello world")
		// http.Redirect(res, req, "/2", 301)
		// https://golang.org/src/net/http/status.go
		http.Redirect(res, req, "/2", http.StatusMovedPermanently)
		// http.Redirect(res, req, "https://google.com.mx", http.StatusMovedPermanently)
	})
	http.HandleFunc("/2", func(res http.ResponseWriter, req *http.Request) {
		// fmt.Fprintf(res, "hello world 2")
		// http.NotFound(res, req)
		http.Error(res, "Este es un error", http.StatusConflict)
	})
	log.Fatal(http.ListenAndServe("localhost:3000", nil)) //correcto
	// log.Fatal(http.ListenAndServe(":3000", nil)) correcto
	// log.Fatal(http.ListenAndServe("localhost:30", nil)) error
	// log.Fatal(http.ListenAndServe("localhosts:3000", nil)) error

}
